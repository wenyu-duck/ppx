package com.ppx.coupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GlscCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(GlscCouponApplication.class, args);
    }

}
